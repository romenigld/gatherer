Rails.application.routes.draw do
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  devise_for :users

  resources :projects

  root to: 'projects#index'
  
  resources :tasks do
    member do
      patch :up
      patch :down
    end
  end

end
