class RenameDueDatainProjectToDueDate < ActiveRecord::Migration

  change_table :projects do |t|
    t.rename :due_data, :due_date
  end

end
